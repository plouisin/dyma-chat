window.addEventListener("DOMContentLoaded", () => {
  const inputChat = document.querySelector(".chat-input");
  const btnChat = document.querySelector(".chat-button");
  inputChat.focus();

  function submitMessage() {
    const value = inputChat.value;
    if (value) {
      ioClient.emit("message", { text: value, roomId: activeRoom._id });
      inputChat.value = "";
      inputChat.focus();
    }
  }

  btnChat.addEventListener("click", submitMessage);

  inputChat.addEventListener("keydown", (event) => {
    ioClient.emit("isTyping", { roomId: activeRoom._id });

    if (event.code === "Enter" || event.code === "NumpadEnter") {
      submitMessage();
    }
  });
});
